import { Component, OnInit, Directive, EventEmitter, Output,ElementRef  } from '@angular/core';
import { Hero } from '../models/hero';
import { HEROES } from '../mocks/mocks-heroes';
import { MainFrameComponent } from '../main-frame/main-frame.component';
@Component({
  selector: 'app-heroes',
  templateUrl: './heroes.component.html',
  styleUrls: ['./heroes.component.css']
})

export class HeroesComponent implements OnInit {

  heroes = HEROES;

  selectedHero: Hero;

  beginpathIcon="assets/portrait/icons/Icon-";
  endPathIcon=".png";

  urlPictoAttaker="assets/portrait/icons/OffenseIcon.png";
  urlPictoDefence ="assets/portrait/icons/DefenseIcon.png";
  urlPictoSupport = "assets/portrait/icons/SupportIcon.png";
  urlPictoTank = "assets/portrait/icons/TankIcon.png";
  
  @Output() init: EventEmitter<ElementRef> = new EventEmitter<ElementRef>();

  constructor(private mainFrameComponent: MainFrameComponent) { }

  ngOnInit() {
  }

  onSelect(hero: Hero): void {
    this.selectedHero = hero;
    this.mainFrameComponent.imgSelectedHero =  "assets/portrait/background/" + hero.name + "-portrait.png";
  }


}

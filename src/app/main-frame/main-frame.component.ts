import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-main-frame',
  templateUrl: './main-frame.component.html',
  styleUrls: ['./main-frame.component.css']
})
export class MainFrameComponent implements OnInit {
  imgURI = "";
  imgSelectedHero = "";

  constructor() { }

  ngOnInit() {
    this.loadBackground();
    this.loadBackgroundMap();
  }

  loadBackground():void {
    this.imgSelectedHero = "assets/portrait/background/Genji-portrait.png";
  }
  loadBackgroundMap():void {
    this.imgURI =  "assets/maps/Dorado.png";
  }
}

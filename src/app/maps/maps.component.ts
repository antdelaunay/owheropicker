import { Component, OnInit } from '@angular/core';
import { Map } from '../models/map';
import { MAPS } from '../mocks/mocks-maps';
import { MainFrameComponent } from '../main-frame/main-frame.component';

@Component({
  selector: 'app-maps',
  templateUrl: './maps.component.html',
  styleUrls: ['./maps.component.css']
})
export class MapsComponent implements OnInit {

  maps = MAPS;
  selectedMap: Map;
  beginpathIcon="assets/maps/";
  endPathIcon=".png";

  constructor(private mainFrameComponent: MainFrameComponent) { }

  ngOnInit() {
  }
  
  onSelect(map: Map): void {
    this.selectedMap = map;
    console.log("map :" + map.shortName);
    this.mainFrameComponent.imgURI =  "assets/maps/" + map.shortName + ".png";
  }

}

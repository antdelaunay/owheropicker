import { Map } from '../models/map';

export const MAPS: Map[] = [
  { id: 1, name: 'Ayutthaya', type:'' ,shortName:'Ayutthaya'},
  { id: 2, name: 'Black Forest', type:'' ,shortName:'Blackforest'},
  { id: 3, name: 'Castillo', type:'' ,shortName:'Castillo'},
  { id: 4, name: 'Château Guillard', type:'' ,shortName:'Chateauguillard'},
  { id: 5, name: 'Dorado', type:'' ,shortName:'Dorado'}
]
